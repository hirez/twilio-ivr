# README #

Ruby + Sinatra + Twilio for somewhat average justice and unfashionable internet memes. Again.

Building:
=========

Install rbenv (or rvm, or don't bother if you've a forgiving distro and a modern Ruby).

bundle install --binstubs

If that doesn't work, install the bundler gem and try again.

ruby ./twilio.rb should start webrick on localhost port 4567

Rackup/unicorn blah blah blah web application serving oh god why are computers, etc.

