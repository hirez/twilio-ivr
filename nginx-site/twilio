upstream app_server_twilio
  server unix:/var/run/unicorn/twilio.sock fail_timeout=0;
}

server {
  listen *:80; 
  server_name twilio.libeljournal.com;

  access_log  /var/log/nginx/twilio.access.log;

  auth_basic   "Restricted";
  auth_basic_user_file  /etc/nginx/include/.htpasswd;

  client_max_body_size 4G;
  keepalive_timeout 5;

  # path for static files
  root /var/tmp;

  # Prefer to serve static files directly from nginx to avoid unnecessary
  # data copies from the application server.
  try_files $uri/index.html $uri.html $uri @app_server_twilio;

  location @app_server_twilio {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass http://app_server_twilio;
  }
}
