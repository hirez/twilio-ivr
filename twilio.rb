#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'

require 'sinatra'
require 'twilio-ruby'


get '/twiml' do
  Twilio::TwiML::Response.new do |r|
    r.Gather :numDigits => '1', :action => '/gather1', :method => 'get' do |g|
      g.Say 'Welcome to amalgamated industries.', :voice => 'alice'
      g.Pause
      g.Say 'For a brief impression that your actions may be meaningful, press 1.', :voice => 'alice'
      g.Say 'For Extreme Noise Terror in session, press 2.', :voice => 'alice'
      g.Say 'To accept delivery of the complete internet cat picture archive, press 3.', :voice => 'alice'
      g.Pause
      g.Say 'I am not even supposed to be here today.'
      g.Pause
      g.Say 'Go and make the tea then. One sugar, please.', :voice => 'alice'
      g.Pause
      g.Say 'Biscuit?'
      g.Pause
      g.Say 'Oh go on then.', :voice => 'alice'
    end
  end.text
end

get '/gather1' do
  redirect '/twiml' if params['Digits'] == '4'
  Twilio::TwiML::Response.new do |r|
    case params['Digits']
    when '1'
      r.Say 'I am afraid that nothing happened.', :voice => 'alice'
    when '2'
      r.Say 'John Peel is revered as a saint among my people.', :voice => 'alice'
    when '3'
      r.Say 'Is is about ethics in games journalism.'
    end
  end.text
end

